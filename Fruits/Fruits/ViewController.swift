//
//  ViewController.swift
//  Fruits
//
//  Created by Isaac Medina Nuez on 12/09/2019.
//  Copyright © 2019 Isaac Medina Nuez. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var getButton: UIButton!
    @IBOutlet weak var fruitsTableView: UITableView!
    @IBOutlet weak var veilView: UIView!
    @IBOutlet weak var loadingFruitsActivityIndicator: UIActivityIndicatorView!
    
    var fruits: Array<Fruit> = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        fruitsTableView.dataSource = self
        fruitsTableView.delegate = self
        fruitsTableView.isHidden = true
        fruitsTableView.separatorStyle = .none
        
        // Setting button style
        getButton.backgroundColor = .init(red: 46/255, green: 184/255, blue: 46/255, alpha: 1)
        getButton.setTitleColor(.white, for: .normal)
        getButton.setTitleColor(.gray, for: .highlighted)
        getButton.layer.cornerRadius = 5
        
        
        // Loading veil and activity indicator
        veilView.backgroundColor = .init(red: 0, green: 0, blue: 0, alpha: 0.5)
        veilView.isHidden = true
        loadingFruitsActivityIndicator.color = .white
    }
    
    struct Fruit: Codable{
        var name: String
        var image: String
        var color: String
    }

    @IBAction func getButtonDidPressed(_ sender: Any) {
        self.veilView.isHidden = false
        
        // API URL
        let apiUrl = URL(string: "http://localhost/fruits/public/api/fruits")!
        
        // Sending request
        let task = URLSession.shared.dataTask(with: apiUrl) { (data, response, error) in
            guard let dataResponse = data,
                error == nil else {
                    print(error?.localizedDescription ?? "Response Error")
                    return }
            do{
                
                let decoder = JSONDecoder()
                self.fruits = try decoder.decode([Fruit].self, from: dataResponse)
                
                DispatchQueue.main.async {
                    self.fruitsTableView.isHidden = false
                    self.fruitsTableView.reloadData()
                    self.veilView.isHidden = true
                }
                
            } catch let parsingError {
                print("Error", parsingError)
            }
        }
        task.resume()
    }
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return fruits.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FruitCell", for: indexPath) as! FruitsTableViewCell
        let uiColor = hexStringToUIColor(hex: fruits[indexPath.row].color)
        
        cell.cellContentView.layer.borderColor = uiColor.cgColor
        cell.fruitNameLabel.text = fruits[indexPath.row].name
        cell.fruitNameLabel.backgroundColor = uiColor
        
        cell.fruitImageView.load(url: URL(string: fruits[indexPath.row].image)!)
        return cell
    }
    
    // Custom functions
    func hexStringToUIColor (hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt64 = 0
        Scanner(string: cString).scanHexInt64(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    
}

extension UIImageView {
    func load(url: URL) {
        DispatchQueue.global().async { [weak self] in
            if let data = try? Data(contentsOf: url) {
                if let image = UIImage(data: data) {
                    DispatchQueue.main.async {
                        self?.image = image
                    }
                }
            }
        }
    }
}
