//
//  FruitsTableViewCell.swift
//  Fruits
//
//  Created by Isaac Medina Nuez on 12/09/2019.
//  Copyright © 2019 Isaac Medina Nuez. All rights reserved.
//

import UIKit

class FruitsTableViewCell: UITableViewCell {

    @IBOutlet weak var cellContentView: UIView!
    @IBOutlet weak var fruitNameLabel: UILabel!
    @IBOutlet weak var fruitImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        selectionStyle = .none
        fruitNameLabel.textColor = .white
        fruitNameLabel.layer.masksToBounds = true
        //fruitNameLabel.layer.cornerRadius = 5
        cellContentView.layer.cornerRadius = 5
        cellContentView.layer.borderWidth = 1
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
